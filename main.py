import classify_image

classify_image.init_tensorflow()

# Classify single image
single_result = classify_image.classify_image('img/selfie.jpg')
print(single_result)

# Classify every image in folder
folder_results = classify_image.classify_all_images('img')